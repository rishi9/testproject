# The Crypto UBI Project (TCUP)

We are developing a cryptocurrency "faucet" to deliver a universal basic income (UBI). As part of the project, we will implement the Kuwa identity sytem.
A high level description of the project can be found [here](http://www.kuwa.org/Kuwa-Driven_Basic_Income_Faucet.pdf).  

The Kuwa white paper can be found [here](https://jamespflynn.com/2018/03/01/kuwa-a-decentralized-pseudo-anonymous-and-sybil-resistant-individual-identification-system/).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Project structure

Under the src folder, the project is split into the following six modules:  
1. Client - Contains the code for the client side application  
2. Smart Contracts - Contains all the smart contracts solidity files used in the project  
3. Sponsor - Contains the code for the Sponsor module  
4. Registrar - Contains the code for the Registrar module  
5. Faucet - Contains the code for the Faucet module  
6. StorageManager - Contains the code for the StorageManager module  


### Prerequisites

You will need to install nodejs and npm
```sudo apt-get install npm```

### Installing

A step by step series of examples that tell you how to get a development env running

Step 1
```
Example
```

```
And more

```
Step 2

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [NodeJS](https://nodejs.org/en/) - The web framework used
* [Web3.js](https://github.com/ethereum/web3.js/) - Ethereum JavaScript API
* [Solidity](solidity.readthedocs.io/) - Used to write Ethereum smart contracts


## Resources

* [API Reference]()
* [Release Notes]()
* [Gallery]()
* [Examples]()
* [Wiki](https://bitbucket.org/rishi9/testproject/wiki/Home)


## Contributing

Please read [CONTRIBUTING.md](https://bitbucket.org/rishi9/testproject/src/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We  plan to use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors and Contributors

* **Jim Flynn** - *Initial work*   
See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License


This project is licensed under the MIT License 

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
